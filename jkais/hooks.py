# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "jkais"
app_title = "JKA Information System"
app_publisher = "littlehera"
app_description = "CDO JKA Trainee Information System"
app_icon = "octicon octicon-file-directory"
app_color = "red"
app_email = "mizushirohera@gmail.com"
app_license = "MIT"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/jkais/css/jkais.css"
# app_include_js = "/assets/jkais/js/jkais.js"

# include js, css files in header of web template
# web_include_css = "/assets/jkais/css/jkais.css"
# web_include_js = "/assets/jkais/js/jkais.js"

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "jkais.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "jkais.install.before_install"
# after_install = "jkais.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "jkais.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"jkais.tasks.all"
# 	],
# 	"daily": [
# 		"jkais.tasks.daily"
# 	],
# 	"hourly": [
# 		"jkais.tasks.hourly"
# 	],
# 	"weekly": [
# 		"jkais.tasks.weekly"
# 	]
# 	"monthly": [
# 		"jkais.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "jkais.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "jkais.event.get_events"
# }

fixtures = ["Custom Field", "Custom Script", "Property Setter", "Print Format", "Report", "Workflow", "Workflow State",
            "Workflow Action"]